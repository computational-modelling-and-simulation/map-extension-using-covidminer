receptor_complex_complex_cell  =  (receptor_complex_complex_cell & Ifn_lambda_complex)
ISGF3_complex  =  (ISGF3_precursor_complex & IRF9)
receptor_complex_complex_cell  =  receptor_complex_complex_cell
receptor_complex_complex_cell  =  (receptor_complex_complex_cell & SOCS3)
Inhibited_ISGF3_precursor_complex  =  (ISGF3_precursor_complex & STAT3)
p38_NFkB_complex  =  (IRF3_phosphorylated)
inhibited_ribosomal_40S_SU_complex  =  (ribosomal_40S_subunit_complex & Nsp1)
inhibited_80S_ribosome_complex  =  (80S_ribosome_complex & Nsp1)
RIG_I_MAVS_complex_mitochondrion  =  ((RIG_I_dsRNA_complex_mitochondrion & MAVS))
ISGF3_precursor_complex  =  ((STAT1_empty & receptor_complex_complex_cell) | (STAT2_empty & receptor_complex_complex_cell))
RIG_I_MAVS_complex_peroxisome  =  dsRNA_rna
80S_ribosome_complex  =  (ribosomal_60S_subunit_complex & ribosomal_40S_subunit_complex)
RIG_1_M_Protein_complex  =  (M & RIG_I)
RIG_I_dsRNA_complex_mitochondrion  =  ((RIG_I & dsRNA_rna) | (RIG_I_dsRNA_complex_mitochondrion & PLPro))
RIG_I_dsRNA_complex_mitochondrion  =  (RIG_I_dsRNA_complex_mitochondrion & Riplet_TRIM25_complex)
Riplet_TRIM25_complex  =  Riplet_TRIM25_complex
ribosomal_60S_subunit_complex  =  ribosomal_60S_subunit_complex
ribosomal_40S_subunit_complex  =  ribosomal_40S_subunit_complex
IFNs_complex  =  (ISGs_rna & 80S_ribosome_complex)
receptor_complex_complex_cell  =  (receptor_complex_complex_cell & SOCS1)
antiviral_proteins_complex  =  (ISGs_rna & 80S_ribosome_complex)
Ifn_lambda_complex  =  (IFN_III | receptor_complex_complex_cell)
STAT1_empty  =  STAT1_empty
STAT2_empty  =  STAT2_empty
IRF9  =  IRF9
STAT3  =  STAT3 | !TNFAIP6
ISGs_rna  =  (IFN_sensitive_response_element_gene & ISGF3_complex)
IFN_sensitive_response_element_gene  =  IFN_sensitive_response_element_gene
SOCS1  =  SOCS1
SOCS3  =  SOCS3
Nsp1  =  Nsp1
viral_RNA_rna  =  viral_RNA_rna
viral_RNA_N_methyl_Guanine_rna  =  (viral_RNA_rna & nsp14)
5_cap_viral_RNA_rna  =  (viral_RNA_N_methyl_Guanine_rna & nsp16)
IFIH1  =  IFIH1
nsp14  =  nsp14
nsp16  =  nsp16
dsRNA_rna  =  (dsRNA_vesicle_rna | RNA_rna)
IRF1_phosphorylated  =  RIG_I_MAVS_complex_peroxisome
IFN_III_rna  =  (p38_NFkB_complex | IRF1_phosphorylated)
IFN_III  =  IFN_III_rna
RIG_I  =  RIG_I
MAVS  =  MAVS
PLPro  =  PLPro
M  =  M
dsRNA_vesicle_rna  =  dsRNA_rna
IRF3_phosphorylated  =  RIG_I_MAVS_complex_mitochondrion
RNA_rna  =  RNA_rna
TNFAIP6  =  TNFAIP6
