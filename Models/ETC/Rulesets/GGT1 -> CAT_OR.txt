Complex_1_complex  =  ((NDUFA1 & NDUFB9 & mtDNA_encoded_OXPHOS_units_complex_mitochondrial_matrix & OXPHOS_factors_complex) | (NADH_simple_molecule & Complex_1_complex))
mtDNA_encoded_OXPHOS_units_complex_mitochondrial_matrix  =  ((Mt_translation_complex | Mt_ribosomal_proteins_complex | Nsp8_affected_Mt_ribosomal_proteins_complex) & mt_mRNA_rna)
OXPHOS_factors_complex  =  (ACAD9 & ECSIT & NDUFAF7)
complex_2_complex_mitochondrial_matrix  =  ((SIRT3 | OXPHOS_factors_complex) | (complex_2_complex_mitochondrial_matrix & Q_simple_molecule_mitochondrial_matrix & Complex_1_complex))
complex_3_complex  =  ((mtDNA_encoded_OXPHOS_units_complex_mitochondrial_matrix & OXPHOS_factors_complex) | (Complex_1_complex & Q_simple_molecule_mitochondrial_matrix))
complex_2_complex_mitochondrial_matrix  =  (complex_2_complex_mitochondrial_matrix & TCA_phenotype)
ATP_Synthase_complex  =  (H__ion_mitochondrion | H__ion_mitochondrion | H__ion_mitochondrion | (ATP5MG & !Nsp6))
mtDNA_encoded_OXPHOS_units_complex_mitochondrial_matrix  =  mtDNA_encoded_OXPHOS_units_complex_mitochondrial_matrix
complex_4_complex  =  ((mtDNA_encoded_OXPHOS_units_complex_mitochondrial_matrix & OXPHOS_factors_complex) | (complex_3_complex & Cyt_C_mitochondrial_matrix))
paraquat_dication_complex  =  (paraquat_simple_molecule & O_2__simple_molecule)
MT_tRNAs_complex  =  ((Mt_tRNA_synthetase_complex | TRMT1) & mt_DNA_gene)
Mt_dNTP_pool_complex  =  Mt_dNTP_pool_complex
Mt_tRNA_synthetase_complex  =  Mt_tRNA_synthetase_complex
Mt_translation_complex  =  Mt_translation_complex
Mt_DNA_repair_complex  =  Mt_DNA_repair_complex
Mt_replication_complex  =  Mt_replication_complex
Nsp8_affected_Mt_ribosomal_proteins_complex  =  !Nsp8
MT_transcription_complex  =  MT_transcription_complex
mtDNA_encoded_OXPHOS_units_complex_mitochondrial_matrix  =  mtDNA_encoded_OXPHOS_units_complex_mitochondrial_matrix
Mt_ribosomal_proteins_complex  =  Mt_ribosomal_proteins_complex
TOM_complex_complex  =  TOM_complex_complex
TIM22_complex_complex  =  (TIMM29 & !Nsp4)
TIM23_complex_complex  =  TIM23_complex_complex
TIM9_TIM10_complex_complex  =  !Nsp4
PRDX_complex  =  PRDX_complex
NADH_simple_molecule  =  NADH_simple_molecule
NAD__simple_molecule  =  (NADH_simple_molecule & Complex_1_complex)
Q_simple_molecule_mitochondrial_matrix  =  (O_2__simple_molecule & Cyt_C_mitochondrial_matrix & H__ion_mitochondrial_matrix & QH_2__simple_molecule_mitochondrial_matrix & complex_3_complex)
Cyt_C_mitochondrial_matrix  =  Cyt_C_mitochondrial_matrix
H__ion_mitochondrion  =  ((H__ion_mitochondrial_matrix & Complex_1_complex) | (O_2__simple_molecule & Cyt_C_mitochondrial_matrix & H__ion_mitochondrial_matrix & QH_2__simple_molecule_mitochondrial_matrix & complex_3_complex) | complex_3_complex)
H__ion_mitochondrial_matrix  =  (ATP_Synthase_complex | (H_2_O_2__simple_molecule & hydroxide_simple_molecule) | (NADH_simple_molecule & Complex_1_complex) | (superoxide_simple_molecule & Fe3__ion))
O__2__endsuper__simple_molecule  =  (O_2__simple_molecule & Cyt_C_mitochondrial_matrix & H__ion_mitochondrial_matrix & QH_2__simple_molecule_mitochondrial_matrix & complex_3_complex)
O_2__simple_molecule  =  (superoxide_simple_molecule & Fe3__ion)
ADP_simple_molecule  =  ADP_simple_molecule
Pi_simple_molecule  =  Pi_simple_molecule
ATP_simple_molecule  =  ((ATP_Synthase_complex | H__ion_mitochondrial_matrix) & ADP_simple_molecule & Pi_simple_molecule)
SIRT3  =  SIRT3
Q_simple_molecule_mitochondrial_matrix  =  Q_simple_molecule_mitochondrial_matrix
QH_2__simple_molecule_mitochondrial_matrix  =  (complex_2_complex_mitochondrial_matrix & Q_simple_molecule_mitochondrial_matrix & Complex_1_complex)
Cyt_C_mitochondrial_matrix  =  (O_2__simple_molecule & Cyt_C_mitochondrial_matrix & H__ion_mitochondrial_matrix & QH_2__simple_molecule_mitochondrial_matrix & complex_3_complex)
QH_2__simple_molecule_mitochondrial_matrix  =  QH_2__simple_molecule_mitochondrial_matrix
superoxide_simple_molecule  =  ((H_2_O_2__simple_molecule & hydroxide_simple_molecule) | O_2__simple_molecule | O_2__simple_molecule | (paraquat_simple_molecule & O_2__simple_molecule))
H_2_O_2__simple_molecule  =  ((superoxide_simple_molecule & H__ion_mitochondrial_matrix & SOD1) | (superoxide_simple_molecule & H__ion_mitochondrial_matrix & SOD2))
H_2_O_simple_molecule  =  ((H_2_O_2__simple_molecule & TXN2 & PRDX_complex) | ((GPX4 | GPX1) & H_2_O_2__simple_molecule & glutathione_simple_molecule) | (H_2_O_2__simple_molecule & CAT) | (H_2_O_2__simple_molecule & hydroxide_simple_molecule))
SOD2  =  SIRT3
ROS_simple_molecule  =  (superoxide_simple_molecule | superoxide_simple_molecule | hydroxide_simple_molecule | H_2_O_2__simple_molecule)
HO_simple_molecule  =  ((H_2_O_2__simple_molecule & Fe2__ion) | (hydroxide_simple_molecule & Fe2__ion))
hydroxide_simple_molecule  =  (H_2_O_2__simple_molecule & Fe2__ion)
Fe2__ion  =  (superoxide_simple_molecule & Fe3__ion)
Fe3__ion  =  ((H_2_O_2__simple_molecule & Fe2__ion) | (hydroxide_simple_molecule & Fe2__ion))
SOD1  =  SOD1
GPX1  =  GPX1
GPX4  =  GPX4
glutathione_disulfide_simple_molecule  =  ((GPX4 | GPX1) & H_2_O_2__simple_molecule & glutathione_simple_molecule)
glutathione_simple_molecule  =  (glutathione_disulfide_simple_molecule & NADPH_simple_molecule & GSR)
NADPH_simple_molecule  =  NADPH_simple_molecule
NADP__simple_molecule  =  ((glutathione_disulfide_simple_molecule & NADPH_simple_molecule & GSR) | (TXN2 & NADPH_simple_molecule & TXNRD2))
GSR  =  GSR
CAT  =  CAT | GGT1
TXN2  =  ((H_2_O_2__simple_molecule & TXN2 & PRDX_complex) | (TXN2 & NADPH_simple_molecule & TXNRD2))
TXNRD2  =  TXNRD2
paraquat_simple_molecule  =  ((Complex_1_complex | superoxide_simple_molecule) & paraquat_dication_complex)
mt_mRNA_rna  =  ((TFAM | MT_transcription_complex) & mt_DNA_gene & !damaged_mt_DNA_gene)
TFAM  =  TFAM
mt_DNA_damage_phenotype  =  mt_DNA_damage_phenotype
mt_DNA_replication_phenotype  =  (TFAM | Mt_replication_complex | Mt_dNTP_pool_complex)
Nsp8  =  Nsp8
Orf9c  =  Orf9c
Nsp7  =  Nsp7
TCA_phenotype  =  TCA_phenotype
mt_DNA_gene  =  ((mt_DNA_replication_phenotype & !mt_DNA_damage_phenotype) | ((TFAM | Mt_DNA_repair_complex | Mt_dNTP_pool_complex) & damaged_mt_DNA_gene))
damaged_mt_DNA_gene  =  (mt_DNA_gene & mt_DNA_damage_phenotype)
NDUFB9  =  !Orf9c
NDUFA1  =  !Orf9c
ECSIT  =  !Orf9c
ACAD9  =  !Orf9c
NDUFAF7  =  !Nsp7
precursor_protein_N_terminus_binding  =  ((TOM_complex_complex | TIM22_complex_complex | TIM23_complex_complex | TIM9_TIM10_complex_complex) & !Orf9b)
Orf9b  =  Orf9b
TRMT1  =  TRMT1
Nsp5  =  Nsp5
Nsp4  =  Nsp4
TIMM29  =  TIMM29
ATP5MG  =  ATP5MG
Nsp6  =  Nsp6
GGT1  =  GGT1
