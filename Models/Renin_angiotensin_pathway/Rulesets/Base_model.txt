                                       ID
v1                                   <NA>
v2                                   <NA>
v3                                   <NA>
v4                       CHEBI:CHEBI:2718
v5                                   <NA>
v6                                   <NA>
v7                                   <NA>
v8                                   <NA>
v9                                   <NA>
v10                                  <NA>
v11                                  <NA>
v12                                  <NA>
v13                                  <NA>
v14                            GeneID:183
v15                     CHEBI:CHEBI:17823
v16                                 CoV:S
v17                                  <NA>
v18                           GeneID:7113
v19                           GeneID:6868
v20                                  <NA>
v21                                  <NA>
v22                                  <NA>
v23                           GeneID:7064
v24                                  <NA>
v25                                  <NA>
v26                                  <NA>
v27                         GO:GO:0007568
v28                         GO:GO:0042310
v29                         GO:GO:0006954
v30                                  <NA>
v31                                  <NA>
v32                                  <NA>
v33                            GeneID:290
v34                                  <NA>
v35                                  <NA>
v36                                  <NA>
v37                           GeneID:5972
v38                                  <NA>
v39                                  <NA>
v40                                  <NA>
v41                                  <NA>
v42                                  <NA>
v43                                  <NA>
v44                                  <NA>
v45                                  <NA>
v46                                  <NA>
v47                           GeneID:5550
v48                                  <NA>
v49                           GeneID:1215
v50                     CHEBI:CHEBI:50113
v51                                  <NA>
v52                           GeneID:1509
v53                           GeneID:1511
v54                                  <NA>
v55                           GeneID:3816
v56                                  <NA>
v57                                  <NA>
v58                                  <NA>
v59                                  <NA>
v60                                  <NA>
v61                                  <NA>
v62                                  <NA>
v63                                  <NA>
v64                                  <NA>
v65                                  <NA>
v66                                  <NA>
v67                          MeSH:D005355
v68                          MeSH:D013927
v69                                  <NA>
v70                                  <NA>
v71                                  <NA>
v72                     CHEBI:CHEBI:27584
v73 TEXT:43f268cb8b6a0438c86037330e0bf860
v74                          MeSH:D006973
v75                                  <NA>
v76                          MeSH:D011654
v77                         GO:GO:0070527
                                          Name
v1                          ACE2:AGTR1_complex
v2                  ACE2-Spike complex_complex
v3                          MAS1:AGTR1_complex
v4               angiotensin I_simple_molecule
v5             angiotensin 1-9_simple_molecule
v6         angiotensin II_simple_molecule_cell
v7        angiotensin 1-7_simple_molecule_cell
v8                           AGTR2_cell_active
v9                                  AGTR1_cell
v10                           MAS1_cell_active
v11                                   ACE_cell
v12                                  ACE2_cell
v13            angiotensin 1-5_simple_molecule
v14                                        AGT
v15                 Calcitriol_simple_molecule
v16                                          S
v17                                   MME_cell
v18                                    TMPRSS2
v19                                     ADAM17
v20                        sex, male_phenotype
v21                     Camostat mesilate_drug
v22                                       PRCP
v23                                      THOP1
v24                 alamandine_simple_molecule
v25                         MRGPRD_cell_active
v26              angiotensin A_simple_molecule
v27                            aging_phenotype
v28                 vasoconstriction_phenotype
v29            inflammatory response_phenotype
v30            angiotensin III_simple_molecule
v31             angiotensin IV_simple_molecule
v32                          ENPEP_cell_active
v33                                     ANPEP 
v34                            Lisinopril_drug
v35                              Losartan_drug
v36                             CGP42112A_drug
v37                                        REN
v38                              ACE2, soluble
v39                                  MAS1_cell
v40                                 AGTR2_cell
v41                                MRGPRD_cell
v42  angiotensin II_simple_molecule_human host
v43 angiotensin 1-7_simple_molecule_human host
v44                           ACE2_cell_active
v45                            ACE_cell_active
v46                          AGTR1_cell_active
v47                                       PREP
v48           angiotensin 1-12_simple_molecule
v49                                       CMA1
v50                   androgen_simple_molecule
v51           ethynylestradiol_simple_molecule
v52                                       CTSD
v53                                       CTSG
v54            angiotensin 1-4_simple_molecule
v55                                       KLK1
v56                                 LNPEP_cell
v57                          LNPEP_cell_active
v58                 oxidative stress_phenotype
v59                              AR234960_drug
v60            angiotensin 3-7_simple_molecule
v61                                QGC001_drug
v62                                 ENPEP_cell
v63             SARS-CoV-2 infection_phenotype
v64          viral replication cycle_phenotype
v65                                  ACE2_gene
v66                  estradiol_simple_molecule
v67                         fibrosis_phenotype
v68                       thrombosis_phenotype
v69                neurodegeneration_phenotype
v70           ABO blood group system_phenotype
v71                            MME_cell_active
v72                aldosterone_simple_molecule
v73                        cognition_phenotype
v74                     hypertension_phenotype
v75       Diabetes mellitus, type II_phenotype
v76                  pulmonary edema_phenotype
v77             platelet aggregation_phenotype
                                         Label
v1                                  ACE2:AGTR1
v2                          ACE2-Spike complex
v3                                  MAS1:AGTR1
v4                               angiotensin I
v5                             angiotensin 1-9
v6         angiotensin II_simple_molecule_cell
v7        angiotensin 1-7_simple_molecule_cell
v8                           AGTR2_cell_active
v9                                  AGTR1_cell
v10                           MAS1_cell_active
v11                                   ACE_cell
v12                                  ACE2_cell
v13                            angiotensin 1-5
v14                                        AGT
v15                                 Calcitriol
v16                                          S
v17                                   MME_cell
v18                                    TMPRSS2
v19                                     ADAM17
v20                                  sex, male
v21                     Camostat mesilate_drug
v22                                       PRCP
v23                                      THOP1
v24                                 alamandine
v25                         MRGPRD_cell_active
v26                              angiotensin A
v27                                      aging
v28                           vasoconstriction
v29                      inflammatory response
v30                            angiotensin III
v31                             angiotensin IV
v32                          ENPEP_cell_active
v33                                     ANPEP 
v34                            Lisinopril_drug
v35                              Losartan_drug
v36                             CGP42112A_drug
v37                                        REN
v38                              ACE2, soluble
v39                                  MAS1_cell
v40                                 AGTR2_cell
v41                                MRGPRD_cell
v42  angiotensin II_simple_molecule_human host
v43 angiotensin 1-7_simple_molecule_human host
v44                           ACE2_cell_active
v45                            ACE_cell_active
v46                          AGTR1_cell_active
v47                                       PREP
v48                           angiotensin 1-12
v49                                       CMA1
v50                                   androgen
v51                           ethynylestradiol
v52                                       CTSD
v53                                       CTSG
v54                            angiotensin 1-4
v55                                       KLK1
v56                                 LNPEP_cell
v57                          LNPEP_cell_active
v58                           oxidative stress
v59                              AR234960_drug
v60                            angiotensin 3-7
v61                                QGC001_drug
v62                                 ENPEP_cell
v63                       SARS-CoV-2 infection
v64                    viral replication cycle
v65                                  ACE2_gene
v66                                  estradiol
v67                                   fibrosis
v68                                 thrombosis
v69                          neurodegeneration
v70                     ABO blood group system
v71                            MME_cell_active
v72                                aldosterone
v73                                  cognition
v74                               hypertension
v75                 Diabetes mellitus, type II
v76                            pulmonary edema
v77                       platelet aggregation
                                                                                              Rule
v1                                                                                (v9 & v12 & !v6)
v2                                                              ((v12 & v16) | (v16 & v18 & !v21))
v3                                                                                     (v46 & v39)
v4                                               ((v14 & v37) | (v48 & v11) | ((v52 | v53) & v14))
v5                                                                                      (v4 & v12)
v6                                      ((v4 & v11) | v16 | (v48 & v49) | (v4 & v49) | (v4 & v55))
v7  (((v45 | v17) & v5) | ((v12 | v22) & v6) | (v4 & v17) | (v48 & v17) | (v4 & v23) | (v4 & v47))
v8                                    ((v40 & v36) | ((v6 | v26) & v40) | (v40 & v7) | (v40 & v5))
v9                                                                                              v9
v10                                                       ((v39 & v13) | (v39 & v59) | (v39 & v7))
v11                                                                                   (v66 & !v15)
v12                        (((v66 | v15) & v65 & !v16) | !v63 | (v65 & v27) | (v65 & !v74 & !v75))
v13                                                                                     (v7 & v45)
v14                                                                                            v51
v15                                                                                            v15
v16                                                                                            v16
v17                                                                                            v17
v18                                                                                            v50
v19                                                                                            v19
v20                                                                                            v20
v21                                                                                            v21
v22                                                                                            v22
v23                                                                                            v23
v24                                                                             (v7 | (v26 & v12))
v25                                                                     ((v41 & v24) | (v41 & v7))
v26                                                                                             v6
v27                                                                                            v27
v28                                                               (!v8 | !v10 | !v57 | !v25 | v46)
v29                                                                     (!v25 | !v10 | !v57 | v46)
v30                                                                                     (v6 & v32)
v31                                                                                    (v30 & v33)
v32                                                                  ((v62 & !v61) | (v62 & !v20))
v33                                                                                            v33
v34                                                                                            v34
v35                                                                                            v35
v36                                                                                            v36
v37                                                                                           !v15
v38                                                                            ((v19 | v46) & v12)
v39                                                                                           !v72
v40                                                                                            v40
v41                                                                                            v41
v42                                                                                            v63
v43                                                                                    (v42 & v38)
v44                                                            ((v12 & v20) | ((v27 | v74) & v12))
v45                 ((v11 & !v27 & !v20) | (v11 & v72) | (v11 & v74) | (v11 & !v34) | (v11 & v70))
v46                              (((v6 | v31 | v26) & v9) | (v9 & !v35) | (v9 & !v7) | (v6 & !v3))
v47                                                                                            v47
v48                                                                                            v48
v49                                                                                            v49
v50                                                                                            v50
v51                                                                                            v51
v52                                                                                            v52
v53                                                                                            v53
v54                                                                                    (v48 & v17)
v55                                                                                            v55
v56                                                                                            v56
v57                                                                    ((v56 & v31) | (v56 & v60))
v58                                                                                   (!v10 | v46)
v59                                                                                            v59
v60                                                                                (v7 | v6 | v31)
v61                                                                                            v61
v62                                                                                            v62
v63                                                                                            v63
v64                                                                                             v2
v65                                                                                            v65
v66                                                                                            v66
v67                                                                             (!v10 | !v8 | v46)
v68                                                                                   (!v10 | v46)
v69                                                                                           !v57
v70                                                                                            v70
v71                                                                                    (v17 & v20)
v72                                                                                            v46
v73                                                                                   (!v46 | v57)
v74                                                                                            v74
v75                                                                                            v75
v76                                                                                            v46
v77                                                                                            v46
