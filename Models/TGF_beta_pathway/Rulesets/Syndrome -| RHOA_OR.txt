BMPR1_2_ACTR2_complex  =  BMPR1_2_ACTR2_complex
SMAD1_5_8_complex  =  ((BMPR1_2_ACTR2_complex | Orf3a | Nsp7b_cell | E) & !BAMBI & !MAPK3)
E3_ubiquitin_ligase_complex_complex  =  E3_ubiquitin_ligase_complex_complex
SMAD2_3_complex  =  ((TGFB_TGFBR_complex | ACVR1 | ACVR1B | Orf3a | Nsp7b_default_compartment) & !BAMBI & !E3_ubiquitin_ligase_complex_complex & !MAPK3)
TGFB_TGFBR_complex  =  (Orf8 & !LTBP1 & !BAMBI)
PP2A_complex  =  (TGFB_TGFBR_complex | Orf7a)
LTBP1  =  LTBP1
MAPK3  =  MAPK3
BAMBI  =  BAMBI
RHOA  =  (TGFB_TGFBR_complex | Nsp7) | !Syndrome
RPS6KB1_empty  =  PP2A_complex
ACVR1  =  ACVR1
ACVR1B  =  ACVR1B
Modulation_of_cell_cycle_phenotype  =  (SMAD1_5_8_complex | SMAD2_3_complex)
Nsp7b_default_compartment  =  Nsp7b_default_compartment
Orf3a  =  Orf3a
E  =  E
Nsp7b_cell  =  Nsp7b_cell
Orf8  =  Orf8
Nsp7  =  Nsp7
Orf7a  =  Orf7a
Syndrome  =  Syndrome
