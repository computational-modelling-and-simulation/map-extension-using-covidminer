lactose_synthetase_complex  =  lactose_synthetase_complex
Nucleoside_diphosphate_kinase_complex_cell  =  Nucleoside_diphosphate_kinase_complex_cell
ribonucleoside_reductase_complex_cell  =  ribonucleoside_reductase_complex_cell
ribonucleoside_reductase_complex_cell  =  ribonucleoside_reductase_complex_cell
Nucleoside_diphosphate_kinase_complex_cell  =  Nucleoside_diphosphate_kinase_complex_cell
SIRT5_Nsp14_complex  =  (SIRT5 & Nsp14_mitochondrion)
GLA_Nsp14_complex  =  (GLA & Nsp14_cell)
IMPDH2_Nsp14_complex  =  (IMPDH2 & Nsp14_cell)
D_Galactose_simple_molecule  =  (((GLB1 | LCT) & Lactose_simple_molecule & H2O_simple_molecule_cell) | (Galacitol_simple_molecule & NADP_simple_molecule & AKR1B1) | ((GLA | GLA_Nsp14_complex) & Melibiose_simple_molecule & H2O_simple_molecule_cell & !Migalastat_drug) | ((GLA | GLA_Nsp14_complex) & Raffinose_simple_molecule & H2O_simple_molecule_cell & !Migalastat_drug) | ((GLA | GLA_Nsp14_complex) & Stachyose_simple_molecule & H2O_simple_molecule_cell & !Migalastat_drug))
_alpha__D_Galactose_simple_molecule  =  (D_Galactose_simple_molecule & GALM)
GALM  =  GALM
_alpha__D_Galactose_1P_simple_molecule  =  (_alpha__D_Galactose_simple_molecule & ATP_simple_molecule & GALK1)
GALK1  =  GALK1
ATP_simple_molecule  =  ATP_simple_molecule
ADP_simple_molecule  =  ((_alpha__D_Galactose_simple_molecule & ATP_simple_molecule & GALK1) | (NAD_simple_molecule & ATP_simple_molecule & NADK) | (Deamino_NAD_simple_molecule & L_Glutamine_simple_molecule & ATP_simple_molecule & H2O_simple_molecule_cell & NADSYN1) | (N_Ribosyl_nicotinamide_simple_molecule & ATP_simple_molecule & NRK1) | (Nicotinate_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & H2O_simple_molecule_cell & ATP_simple_molecule & NAPRT1) | (5_phospho_beta_D_ribosylamine_simple_molecule & Glycine_simple_molecule & ATP_simple_molecule & GART) | (5_phosphoribosyl_N_formylglycinamide_simple_molecule & L_Glutamine_simple_molecule & ATP_simple_molecule & H2O_simple_molecule_cell & PFAS) | (2_Formamido_N1_5_phosphoribosyl_acetamidine_simple_molecule & ATP_simple_molecule & GART) | (1_5_Phospho_D_ribosyl_5_amino_4_imidazolecarboxylate_simple_molecule & L_Aspartate_simple_molecule & ATP_simple_molecule & PAICS) | (GMP_simple_molecule & ATP_simple_molecule & GUK1) | ((NME3 | Nucleoside_diphosphate_kinase_complex_cell | NME5 | NME6 | NME7) & GDP_simple_molecule & ATP_simple_molecule) | ((NME3 | Nucleoside_diphosphate_kinase_complex_cell | NME7 | NME6 | NME5) & dGDP_simple_molecule & ATP_simple_molecule) | (dGMP_simple_molecule & ATP_simple_molecule & GUK1) | (Deoxyguanosine_simple_molecule & ATP_simple_molecule & DCK) | (Adenosine_simple_molecule & ATP_simple_molecule & ADK) | ((AK7 | AK1 | AK8 | AK5) & AMP_simple_molecule & ATP_simple_molecule) | (Deoxyadenosine_simple_molecule & ATP_simple_molecule & DCK) | (dAMP_simple_molecule & ATP_simple_molecule & AK5) | ((Nucleoside_diphosphate_kinase_complex_cell | NME5 | NME3 | NME6 | NME7) & dADP_simple_molecule & ATP_simple_molecule) | ((AK7 | AK1 | AK8 | AK5) & AMP_simple_molecule & ATP_simple_molecule))
UDP__alpha__D_Galactose_simple_molecule  =  ((_alpha__D_Galactose_1P_simple_molecule & UDP__alpha__D_Glucose_simple_molecule & GALT) | (UDP__alpha__D_Glucose_simple_molecule & GALE))
UDP__alpha__D_Glucose_simple_molecule  =  UDP__alpha__D_Glucose_simple_molecule
_alpha__D_Glucose_1_P_simple_molecule  =  ((_alpha__D_Galactose_1P_simple_molecule & UDP__alpha__D_Glucose_simple_molecule & GALT) | (UDP__alpha__D_Glucose_simple_molecule & PPi_simple_molecule & UGP2))
GALT  =  GALT
UGP2  =  UGP2
UTP_simple_molecule  =  (UDP__alpha__D_Glucose_simple_molecule & PPi_simple_molecule & UGP2)
PPi_simple_molecule  =  (((NMNAT2 | NMNAT1 | NMNAT3) & Nicotinamide_D_ribonucleotide_simple_molecule & ATP_simple_molecule & H_simple_molecule) | (Nicotinamide_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & NAMPT) | ((NMNAT1 | NMNAT2 | NMNAT3) & Nicotinate_D_ribonucleotide_simple_molecule & H_simple_molecule & ATP_simple_molecule) | (Quinolinate_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & H_simple_molecule & H_simple_molecule & QPRT) | (Nicotinate_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & H2O_simple_molecule_cell & ATP_simple_molecule & NAPRT1) | (5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & H2O_simple_molecule_cell & L_Glutamine_simple_molecule & PPAT) | (XMP_simple_molecule & H2O_simple_molecule_cell & ATP_simple_molecule & L_Glutamine_simple_molecule & GMPS) | ((ITPA | ENPP1 | ENPP3) & dGTP_simple_molecule & H2O_simple_molecule_cell) | (Guanine_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & HPRT1) | (Hypoxanthine_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & HPRT1) | (Adenine_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & APRT))
GALE  =  GALE
Lactose_simple_molecule  =  (UDP__alpha__D_Galactose_simple_molecule & _alpha__D_Glucose_simple_molecule & lactose_synthetase_complex)
_alpha__D_Glucose_simple_molecule  =  (((GLB1 | LCT) & Lactose_simple_molecule & H2O_simple_molecule_cell) | ((GLA | GLA_Nsp14_complex) & Melibiose_simple_molecule & H2O_simple_molecule_cell & !Migalastat_drug))
UDP_simple_molecule  =  (UDP__alpha__D_Galactose_simple_molecule & _alpha__D_Glucose_simple_molecule & lactose_synthetase_complex)
GLB1  =  GLB1
H2O_simple_molecule_cell  =  ((1_5_Phosphoribosyl_5_formamido_4_imidazolecarboxamide_simple_molecule & ATIC) | (GDP_simple_molecule & Thioredoxin_simple_molecule & ribonucleoside_reductase_complex_cell) | (ADP_simple_molecule & Thioredoxin_simple_molecule & ribonucleoside_reductase_complex_cell))
Galacitol_simple_molecule  =  Galacitol_simple_molecule
AKR1B1  =  AKR1B1
NADP_simple_molecule  =  ((NAD_simple_molecule & ATP_simple_molecule & NADK) | (NAD_simple_molecule & NADPH_simple_molecule & NNT))
NADPH_simple_molecule  =  ((Galacitol_simple_molecule & NADP_simple_molecule & AKR1B1) | ((GMPR | GMPR2) & IMP_simple_molecule & Ammonium_simple_molecule & NADP_simple_molecule))
H_simple_molecule  =  ((Galacitol_simple_molecule & NADP_simple_molecule & AKR1B1) | (NAD_simple_molecule & ATP_simple_molecule & NADK) | (Deamino_NAD_simple_molecule & L_Glutamine_simple_molecule & ATP_simple_molecule & H2O_simple_molecule_cell & NADSYN1) | (N_Ribosyl_nicotinamide_simple_molecule & ATP_simple_molecule & NRK1) | (NAD_simple_molecule & H2O_simple_molecule_cell & CD38) | (5_phospho_beta_D_ribosylamine_simple_molecule & Glycine_simple_molecule & ATP_simple_molecule & GART) | (5_phospho_beta_D_ribosylglycinamide_simple_molecule & 10_Formyltetrahydrofolate_simple_molecule & GART) | (5_phosphoribosyl_N_formylglycinamide_simple_molecule & L_Glutamine_simple_molecule & ATP_simple_molecule & H2O_simple_molecule_cell & PFAS) | (2_Formamido_N1_5_phosphoribosyl_acetamidine_simple_molecule & ATP_simple_molecule & GART) | (Aminoimidazole_ribotide_simple_molecule & CO2_simple_molecule & PAICS) | (Aminoimidazole_ribotide_simple_molecule & CO2_simple_molecule & PAICS) | (1_5_Phospho_D_ribosyl_5_amino_4_imidazolecarboxylate_simple_molecule & L_Aspartate_simple_molecule & ATP_simple_molecule & PAICS) | ((IMPDH1 | IMPDH2 | IMPDH2_Nsp14_complex) & IMP_simple_molecule & NAD_simple_molecule & H2O_simple_molecule_cell & !Mycophenolic_acid_drug & !Merimepodib_drug & !Ribavirin_drug) | (XMP_simple_molecule & H2O_simple_molecule_cell & ATP_simple_molecule & L_Glutamine_simple_molecule & GMPS) | (XMP_simple_molecule & H2O_simple_molecule_cell & ATP_simple_molecule & L_Glutamine_simple_molecule & GMPS) | (Deoxyguanosine_simple_molecule & ATP_simple_molecule & DCK) | ((ITPA | ENPP1 | ENPP3) & dGTP_simple_molecule & H2O_simple_molecule_cell) | (GTP_simple_molecule & H2O_simple_molecule_cell & ENTPD2) | (GTP_simple_molecule & H2O_simple_molecule_cell & ENTPD2) | ((ENTPD2 | NTPCR) & GTP_simple_molecule & H2O_simple_molecule_cell) | ((ENTPD2 | ENTPD4 | ENTPD5 | ENTPD6 | CANT1) & GDP_simple_molecule & H2O_simple_molecule_cell) | ((GMPR | GMPR2) & IMP_simple_molecule & Ammonium_simple_molecule & NADP_simple_molecule) | ((GMPR | GMPR2) & IMP_simple_molecule & Ammonium_simple_molecule & NADP_simple_molecule) | (Hypoxanthine_simple_molecule & NAD_simple_molecule & H2O_simple_molecule_cell & XDH) | (Adenosine_simple_molecule & ATP_simple_molecule & ADK) | (Deoxyadenosine_simple_molecule & ATP_simple_molecule & DCK))
Melibiose_simple_molecule  =  Melibiose_simple_molecule
Nsp14_cell  =  Nsp14_cell
Raffinose_simple_molecule  =  ((GLA | GLA_Nsp14_complex) & Stachyose_simple_molecule & H2O_simple_molecule_cell & !Migalastat_drug)
Sucrose_simple_molecule  =  ((GLA | GLA_Nsp14_complex) & Raffinose_simple_molecule & H2O_simple_molecule_cell & !Migalastat_drug)
Stachyose_simple_molecule  =  Stachyose_simple_molecule
GLA  =  GLA
LCT  =  LCT
Migalastat_drug  =  Migalastat_drug
Nicotinamide_simple_molecule  =  ((Nicotinate_simple_molecule & NADP_simple_molecule & CD38) | (N_Ribosyl_nicotinamide_simple_molecule & Pi_simple_molecule & PNP) | ((SIRT5 | SIRT5_Nsp14_complex) & NAD_simple_molecule & Histone_N6_acetyl_L_lysine_simple_molecule & H2O_simple_molecule_mitochondrion) | (NAD_simple_molecule & H2O_simple_molecule_cell & CD38) | Nicotinate_simple_molecule)
N_Ribosyl_nicotinamide_simple_molecule  =  (Nicotinamide_D_ribonucleotide_simple_molecule & H2O_simple_molecule_cell & NT5E)
Nicotinamide_D_ribonucleotide_simple_molecule  =  (((ENPP1 | ENPP3) & NAD_simple_molecule & H2O_simple_molecule_cell) | (N_Ribosyl_nicotinamide_simple_molecule & ATP_simple_molecule & NRK1) | (Nicotinamide_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & NAMPT))
NAD_simple_molecule  =  ((Deamino_NAD_simple_molecule & L_Glutamine_simple_molecule & ATP_simple_molecule & H2O_simple_molecule_cell & NADSYN1) | ((NMNAT2 | NMNAT1 | NMNAT3) & Nicotinamide_D_ribonucleotide_simple_molecule & ATP_simple_molecule & H_simple_molecule))
Deamino_NAD_simple_molecule  =  ((NMNAT1 | NMNAT2 | NMNAT3) & Nicotinate_D_ribonucleotide_simple_molecule & H_simple_molecule & ATP_simple_molecule)
Nicotinate_D_ribonucleotide_simple_molecule  =  ((Quinolinate_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & H_simple_molecule & H_simple_molecule & QPRT) | (Nicotinate_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & H2O_simple_molecule_cell & ATP_simple_molecule & NAPRT1))
NADK  =  NADK
NADH_simple_molecule  =  ((NAD_simple_molecule & NADPH_simple_molecule & NNT) | ((IMPDH1 | IMPDH2 | IMPDH2_Nsp14_complex) & IMP_simple_molecule & NAD_simple_molecule & H2O_simple_molecule_cell & !Mycophenolic_acid_drug & !Merimepodib_drug & !Ribavirin_drug) | (Hypoxanthine_simple_molecule & NAD_simple_molecule & H2O_simple_molecule_cell & XDH))
NNT  =  NNT
NADSYN1  =  NADSYN1
L_Glutamine_simple_molecule  =  L_Glutamine_simple_molecule
L_Glutamate_simple_molecule  =  ((Deamino_NAD_simple_molecule & L_Glutamine_simple_molecule & ATP_simple_molecule & H2O_simple_molecule_cell & NADSYN1) | (5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & H2O_simple_molecule_cell & L_Glutamine_simple_molecule & PPAT) | (5_phosphoribosyl_N_formylglycinamide_simple_molecule & L_Glutamine_simple_molecule & ATP_simple_molecule & H2O_simple_molecule_cell & PFAS) | (XMP_simple_molecule & H2O_simple_molecule_cell & ATP_simple_molecule & L_Glutamine_simple_molecule & GMPS))
NMNAT2  =  NMNAT2
NMNAT1  =  NMNAT1
NMNAT3  =  NMNAT3
ENPP1  =  ENPP1
ENPP3  =  ENPP3
AMP_simple_molecule  =  (((ENPP1 | ENPP3) & NAD_simple_molecule & H2O_simple_molecule_cell) | ((PRPS1 | PRPS2 | PRPS1L1) & D_Ribose_5P_simple_molecule & ATP_simple_molecule) | (XMP_simple_molecule & H2O_simple_molecule_cell & ATP_simple_molecule & L_Glutamine_simple_molecule & GMPS) | (Adenosine_simple_molecule & ATP_simple_molecule & ADK) | (Adenine_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & APRT))
Pi_simple_molecule  =  ((Nicotinamide_D_ribonucleotide_simple_molecule & H2O_simple_molecule_cell & NT5E) | (Nicotinate_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & H2O_simple_molecule_cell & ATP_simple_molecule & NAPRT1) | (5_phospho_beta_D_ribosylamine_simple_molecule & Glycine_simple_molecule & ATP_simple_molecule & GART) | (5_phosphoribosyl_N_formylglycinamide_simple_molecule & L_Glutamine_simple_molecule & ATP_simple_molecule & H2O_simple_molecule_cell & PFAS) | (2_Formamido_N1_5_phosphoribosyl_acetamidine_simple_molecule & ATP_simple_molecule & GART) | (1_5_Phospho_D_ribosyl_5_amino_4_imidazolecarboxylate_simple_molecule & L_Aspartate_simple_molecule & ATP_simple_molecule & PAICS) | (GMP_simple_molecule & H2O_simple_molecule_cell & NT5E) | (GTP_simple_molecule & H2O_simple_molecule_cell & ENTPD2) | (GTP_simple_molecule & H2O_simple_molecule_cell & ENTPD2) | ((ENTPD2 | NTPCR) & GTP_simple_molecule & H2O_simple_molecule_cell) | ((ENTPD2 | ENTPD4 | ENTPD5 | ENTPD6 | CANT1) & GDP_simple_molecule & H2O_simple_molecule_cell) | (XMP_simple_molecule & H2O_simple_molecule_cell & NT5E) | (IMP_simple_molecule & H2O_simple_molecule_cell & NT5E) | (AMP_simple_molecule & H2O_simple_molecule_cell & NT5E) | (Hypoxanthine_simple_molecule & 2_deoxy__alpha__D_ribose_1_phosphate_simple_molecule & PNP))
NT5E  =  NT5E
NRK1  =  NRK1
nicotinate_adenine_dinucleotide_phosphate_simple_molecule  =  (Nicotinate_simple_molecule & NADP_simple_molecule & CD38)
NAMPT  =  NAMPT
5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule  =  ((PRPS1 | PRPS2 | PRPS1L1) & D_Ribose_5P_simple_molecule & ATP_simple_molecule)
_alpha__D_Ribose_1_phosphate_simple_molecule  =  ((N_Ribosyl_nicotinamide_simple_molecule & Pi_simple_molecule & PNP) | (Guanosine_simple_molecule & Pi_simple_molecule & PNP) | (Xanthosine_simple_molecule & Pi_simple_molecule & PNP) | (Inosine_simple_molecule & Pi_simple_molecule & PNP) | (Adenosine_simple_molecule & Pi_simple_molecule & PNP))
PNP  =  PNP
Histone_N6_acetyl_L_lysine_simple_molecule  =  Histone_N6_acetyl_L_lysine_simple_molecule
Histone_L_lysine_simple_molecule  =  ((SIRT5 | SIRT5_Nsp14_complex) & NAD_simple_molecule & Histone_N6_acetyl_L_lysine_simple_molecule & H2O_simple_molecule_mitochondrion)
O_Acetyl_ADP_ribose_simple_molecule  =  ((SIRT5 | SIRT5_Nsp14_complex) & NAD_simple_molecule & Histone_N6_acetyl_L_lysine_simple_molecule & H2O_simple_molecule_mitochondrion)
H2O_simple_molecule_mitochondrion  =  H2O_simple_molecule_mitochondrion
Nsp14_mitochondrion  =  Nsp14_mitochondrion
CD38  =  CD38
ADP_D_ribose_simple_molecule  =  (NAD_simple_molecule & H2O_simple_molecule_cell & CD38)
Quinolinate_simple_molecule  =  Quinolinate_simple_molecule
QPRT  =  QPRT
CO2_simple_molecule  =  (Quinolinate_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & H_simple_molecule & H_simple_molecule & QPRT)
Nicotinate_simple_molecule  =  Nicotinate_simple_molecule
NAPRT1  =  NAPRT1
_alpha_D_Ribose_1P_simple_molecule  =  _alpha_D_Ribose_1P_simple_molecule
D_Ribose_5P_simple_molecule  =  (_alpha_D_Ribose_1P_simple_molecule & PGM2)
PGM2  =  PGM2
PRPS1  =  PRPS1
PRPS2  =  PRPS2
PRPS1L1  =  PRPS1L1
5_phospho_beta_D_ribosylamine_simple_molecule  =  (5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & H2O_simple_molecule_cell & L_Glutamine_simple_molecule & PPAT)
PPAT  =  PPAT
5_phospho_beta_D_ribosylglycinamide_simple_molecule  =  (5_phospho_beta_D_ribosylamine_simple_molecule & Glycine_simple_molecule & ATP_simple_molecule & GART)
GART  =  GART
Glycine_simple_molecule  =  Glycine_simple_molecule
5_phosphoribosyl_N_formylglycinamide_simple_molecule  =  (5_phospho_beta_D_ribosylglycinamide_simple_molecule & 10_Formyltetrahydrofolate_simple_molecule & GART)
10_Formyltetrahydrofolate_simple_molecule  =  10_Formyltetrahydrofolate_simple_molecule
Tetrahydrofolate_simple_molecule  =  ((5_phospho_beta_D_ribosylglycinamide_simple_molecule & 10_Formyltetrahydrofolate_simple_molecule & GART) | (1_5_Phosphoribosyl_5_amino_4_imidazolecarboxamide_simple_molecule & 10_Formyltetrahydrofolate_simple_molecule & ATIC))
2_Formamido_N1_5_phosphoribosyl_acetamidine_simple_molecule  =  (5_phosphoribosyl_N_formylglycinamide_simple_molecule & L_Glutamine_simple_molecule & ATP_simple_molecule & H2O_simple_molecule_cell & PFAS)
PFAS  =  PFAS
Aminoimidazole_ribotide_simple_molecule  =  (2_Formamido_N1_5_phosphoribosyl_acetamidine_simple_molecule & ATP_simple_molecule & GART)
1_5_Phospho_D_ribosyl_5_amino_4_imidazolecarboxylate_simple_molecule  =  (Aminoimidazole_ribotide_simple_molecule & CO2_simple_molecule & PAICS)
PAICS  =  PAICS
1_5_Phosphoribosyl_5_amino_4_N_succinocarboxamide_imidazole_simple_molecule  =  (1_5_Phospho_D_ribosyl_5_amino_4_imidazolecarboxylate_simple_molecule & L_Aspartate_simple_molecule & ATP_simple_molecule & PAICS)
L_Aspartate_simple_molecule  =  L_Aspartate_simple_molecule
1_5_Phosphoribosyl_5_amino_4_imidazolecarboxamide_simple_molecule  =  (1_5_Phosphoribosyl_5_amino_4_N_succinocarboxamide_imidazole_simple_molecule & ADSL)
Fumarate_simple_molecule  =  (1_5_Phosphoribosyl_5_amino_4_N_succinocarboxamide_imidazole_simple_molecule & ADSL)
ADSL  =  ADSL
1_5_Phosphoribosyl_5_formamido_4_imidazolecarboxamide_simple_molecule  =  (1_5_Phosphoribosyl_5_amino_4_imidazolecarboxamide_simple_molecule & 10_Formyltetrahydrofolate_simple_molecule & ATIC)
ATIC  =  ATIC
IMP_simple_molecule  =  ((1_5_Phosphoribosyl_5_formamido_4_imidazolecarboxamide_simple_molecule & ATIC) | ((AMDP2 | AMPD1 | AMPD3) & AMP_simple_molecule & H_simple_molecule & H2O_simple_molecule_cell) | (Hypoxanthine_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & HPRT1))
XMP_simple_molecule  =  ((IMPDH1 | IMPDH2 | IMPDH2_Nsp14_complex) & IMP_simple_molecule & NAD_simple_molecule & H2O_simple_molecule_cell & !Mycophenolic_acid_drug & !Merimepodib_drug & !Ribavirin_drug)
IMPDH2  =  IMPDH2
IMPDH1  =  IMPDH1
Merimepodib_drug  =  Merimepodib_drug
Mycophenolic_acid_drug  =  Mycophenolic_acid_drug
Ribavirin_drug  =  Ribavirin_drug
GMP_simple_molecule  =  ((XMP_simple_molecule & H2O_simple_molecule_cell & ATP_simple_molecule & L_Glutamine_simple_molecule & GMPS) | (Guanine_simple_molecule & 5_phospho__alpha__D_ribose_1_diphosphate_simple_molecule & HPRT1) | (GTP_simple_molecule & H2O_simple_molecule_cell & ENTPD2) | ((ENTPD2 | ENTPD4 | ENTPD5 | ENTPD6 | CANT1) & GDP_simple_molecule & H2O_simple_molecule_cell) | ((GMPR | GMPR2) & IMP_simple_molecule & Ammonium_simple_molecule & NADP_simple_molecule))
GMPS  =  GMPS
GDP_simple_molecule  =  ((GMP_simple_molecule & ATP_simple_molecule & GUK1) | ((ENTPD2 | NTPCR) & GTP_simple_molecule & H2O_simple_molecule_cell))
GUK1  =  GUK1
GTP_simple_molecule  =  ((NME3 | Nucleoside_diphosphate_kinase_complex_cell | NME5 | NME6 | NME7) & GDP_simple_molecule & ATP_simple_molecule)
NME3  =  NME3
NME5  =  NME5
NME6  =  NME6
NME7  =  NME7
dGDP_simple_molecule  =  ((GDP_simple_molecule & Thioredoxin_simple_molecule & ribonucleoside_reductase_complex_cell) | (dGMP_simple_molecule & ATP_simple_molecule & GUK1))
Thioredoxin_disulfide_simple_molecule  =  ((GDP_simple_molecule & Thioredoxin_simple_molecule & ribonucleoside_reductase_complex_cell) | (ADP_simple_molecule & Thioredoxin_simple_molecule & ribonucleoside_reductase_complex_cell))
Thioredoxin_simple_molecule  =  Thioredoxin_simple_molecule
dGTP_simple_molecule  =  ((NME3 | Nucleoside_diphosphate_kinase_complex_cell | NME7 | NME6 | NME5) & dGDP_simple_molecule & ATP_simple_molecule)
dGMP_simple_molecule  =  ((Deoxyguanosine_simple_molecule & ATP_simple_molecule & DCK) | ((ITPA | ENPP1 | ENPP3) & dGTP_simple_molecule & H2O_simple_molecule_cell))
Deoxyguanosine_simple_molecule  =  Deoxyguanosine_simple_molecule
DCK  =  DCK
Guanine_simple_molecule  =  ((Deoxyguanosine_simple_molecule & Pi_simple_molecule & PNP) | (Guanosine_simple_molecule & Pi_simple_molecule & PNP))
Guanosine_simple_molecule  =  (GMP_simple_molecule & H2O_simple_molecule_cell & NT5E)
2_deoxy__alpha__D_ribose_1_phosphate_simple_molecule  =  (Deoxyguanosine_simple_molecule & Pi_simple_molecule & PNP)
ITPA  =  ITPA
HPRT1  =  HPRT1
ENTPD2  =  ENTPD2
NTPCR  =  NTPCR
ENTPD4  =  ENTPD4
ENTPD5  =  ENTPD5
ENTPD6  =  ENTPD6
CANT1  =  CANT1
Ammonium_simple_molecule  =  (((AMDP2 | AMPD1 | AMPD3) & AMP_simple_molecule & H_simple_molecule & H2O_simple_molecule_cell) | (Guanine_simple_molecule & H2O_simple_molecule_cell & H_simple_molecule & GDA) | (Adenosine_simple_molecule & H2O_simple_molecule_cell & H_simple_molecule & ADA) | (Deoxyadenosine_simple_molecule & H2O_simple_molecule_cell & H_simple_molecule & ADA))
GMPR  =  GMPR
GMPR2  =  GMPR2
AMPD1  =  AMPD1
AMDP2  =  AMDP2
AMPD3  =  AMPD3
Xanthosine_simple_molecule  =  (XMP_simple_molecule & H2O_simple_molecule_cell & NT5E)
Xanthine_simple_molecule  =  ((Xanthosine_simple_molecule & Pi_simple_molecule & PNP) | (Hypoxanthine_simple_molecule & NAD_simple_molecule & H2O_simple_molecule_cell & XDH) | (Guanine_simple_molecule & H2O_simple_molecule_cell & H_simple_molecule & GDA))
Inosine_simple_molecule  =  ((IMP_simple_molecule & H2O_simple_molecule_cell & NT5E) | (Adenosine_simple_molecule & H2O_simple_molecule_cell & H_simple_molecule & ADA))
Hypoxanthine_simple_molecule  =  (Inosine_simple_molecule & Pi_simple_molecule & PNP)
XDH  =  XDH
GDA  =  GDA
Adenosine_simple_molecule  =  (AMP_simple_molecule & H2O_simple_molecule_cell & NT5E)
ADA  =  ADA & DPP4
ADK  =  ADK
Adenine_simple_molecule  =  (Adenosine_simple_molecule & Pi_simple_molecule & PNP)
APRT  =  APRT
Deoxyinosine_simple_molecule  =  ((Hypoxanthine_simple_molecule & 2_deoxy__alpha__D_ribose_1_phosphate_simple_molecule & PNP) | (Deoxyadenosine_simple_molecule & H2O_simple_molecule_cell & H_simple_molecule & ADA))
Deoxyadenosine_simple_molecule  =  Deoxyadenosine_simple_molecule
dAMP_simple_molecule  =  (Deoxyadenosine_simple_molecule & ATP_simple_molecule & DCK)
dADP_simple_molecule  =  ((dAMP_simple_molecule & ATP_simple_molecule & AK5) | (ADP_simple_molecule & Thioredoxin_simple_molecule & ribonucleoside_reductase_complex_cell))
AK5  =  AK5
dATP_simple_molecule  =  ((Nucleoside_diphosphate_kinase_complex_cell | NME5 | NME3 | NME6 | NME7) & dADP_simple_molecule & ATP_simple_molecule)
AK8  =  AK8
AK7  =  AK7
AK1  =  AK1
SIRT5  =  SIRT5
Guanine_nucleotide_synthesis_phenotype  =  IMPDH2
Urea_cycle_phenotype  =  SIRT5
DPP4  =  DPP4
