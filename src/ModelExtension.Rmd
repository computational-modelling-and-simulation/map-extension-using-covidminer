---
title: "COVIDminer Model Extension"
output:
  html_document:
    css: style.css
    section_divs: yes
params:
  map: ""
  update: FALSE
  simulations: 0
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
if(length(find.package('COVIDminerAPI', quiet=TRUE)) == 0) devtools::install_github("rupertoverall/COVIDminerAPI", build_vignettes = TRUE)
root.dir = "../"
source(paste0(root.dir, "src/get_modified_model.R"))
source(paste0(root.dir, "src/get_expressions.R"))
source(paste0(root.dir, "src/get_attractor.R"))
source(paste0(root.dir, "src/sbml2network.R"))
source(paste0(root.dir, "src/parse_mathml.R"))
source(paste0(root.dir, "src/assemble_network.R"))
source(paste0(root.dir, "src/network_union.R"))
source(paste0(root.dir, "src/plot_matrix.R"))
source(paste0(root.dir, "src/colour_tools.R"))

connected = function() {
  tryCatch({
    readLines("https://rupertoverall.net/covidminer/", n = 1)
    TRUE
  },
  warning = function(w) invokeRestart("muffleWarning"),
  error = function(e) FALSE)
}

map.name = params$map
map = gsub("[^a-zA-Z0-9_]+", "_", map.name)
update = params$update
simulations = params$simulations # Number of randomised starting states per input node.
edge.list = NULL
message(paste0("Running ModelExtension for the ", map, " map with ", c("no", "full")[update + 1], " updating and ", simulations, " model simulations."))
```
<script src="../js/cytoscape.min.js"></script>
<script src="../js/jquery-3.1.1.min.js"></script>
<script src="../js/fcose.js"></script>

This report is based on the `r map.name` map from the [COVID-19 Disease Map](https://covid.pages.uni.lu/) project.
The text mining resource [COVIDminer](https://rupertoverall.net/covidminer/) has be used to discover novel interactions, mentioned in the scientific literature, that might be candidates for addition to the map.

<hr>

```{r readme, echo=FALSE, results='hide'}
readme = RCurl::getURL(paste0("https://git-r3lab.uni.lu/covid/models/-/raw/master/Curation/", map.name, "/README.md?inline=false"))
if(grepl("^<!DOCTYPE html>", readme)) readme = "" # No README file for this project.
```

`r readme`

```{r sbmlconversion, echo=FALSE, results='hide'}
# Get file listing.
listing = read.delim(paste0(root.dir, "Maps/Listing.txt"))
xmlpath = listing[listing$Map == map.name, "XML"]

if(!dir.exists(paste0(root.dir, "/Maps/", map))) dir.create(paste0(root.dir, "/Maps/", map))

# Get map as map. Regardless of the actual filename in gitlab (xmlpath), rename all local files to the map name for convenience.
if(update | !file.exists(paste0(root.dir, "Maps/", map, "/", map, ".xml"))) download.file(xmlpath, paste0(root.dir, "Maps/", map, "/", map, ".xml"))
# Convert to SBML model.
if(update | !file.exists(paste0(root.dir, "Maps/", map, "/", map, ".sbml"))) system2("casq", args = c(paste0(root.dir, "Maps/", map, "/", map, ".xml"), paste0(root.dir, "Maps/", map, "/", map, ".sbml")))
message("SBML conversion complete.")
```

```{r basenetworkpreassembly, echo=FALSE, results='hide'}
# Load SBML as network (including model rules and coordinates).
base.network = sbml2network(paste0(root.dir, "Maps/", map, "/", map, ".sbml"))
base.network$vertices$ID = gsub("[^a-zA-Z0-9_]+", "_", base.network$vertices$Name) # This is what BoolNet does.
# Remove class tags.
base.network$vertices$Label = sub("_phenotype$|_complex$|_simple_molecule$", "", base.network$vertices$Name)
# Add guessed classes.
base.network$vertices$Class = "gene"
base.network$vertices$Class[grepl("_phenotype$", base.network$vertices$Name)] = "trait"
base.network$vertices$Class[grepl("_complex$", base.network$vertices$Name)] = "complex"
base.network$vertices$Class[grepl("_simple_molecule$", base.network$vertices$Name)] = "chemical"
base.network$edges$model = "original"
# Extract just the model components to work with.
model = base.network$vertices[, c("ID", "Name", "Label", "Rule")] # ID will be replaced with CMID.
```

```{r simulationparametersetup, echo=FALSE, results='hide'}
# Set up simulation parameters.
# If there is a novel input node, use the same starting state for each run. If not, ignore it.
is.input = c(sapply(rownames(model), function(i) unname(model[i, "Rule"] == i) ), "vnew" = TRUE) 
input.nodes = names(which(is.input))
output.nodes = names(which(sapply(rownames(model), function(v) !any(grepl(v, model$Rule)) )))
# n.perm = length(input.nodes) * simulations # Number of permuted starting states (a subset of the total starting state space).
# set.seed(123) # The results should be reproducible.
# # Pre-calculate the permuted starting states so that each modified model is comparable.
# input.states = lapply(is.input, function(i) if(i) sample(c(0, 1), n.perm, replace = TRUE) else rep(0, n.perm) )
## TODO The modelling is temporarily bypassed until we can establish some battle-tested starting states.
## The full search space is impractically large at present.
n.perm = 1
input.states = lapply(is.input, function(i) rep(1, n.perm) ) # Set ALL nodes to TRUE.
input.states = t(as.data.frame(input.states[c(rownames(model), "vnew")])) # A new input, if present, is always added by 'get_modified_model()' as the last gene.
if(!dir.exists(paste0(root.dir, "Models/", map))) dir.create(paste0(root.dir, "Models/", map))
```

```{r covidminersearch, echo=FALSE, results='hide'}
# Search for matching COVIDminer IDs for each of the model nodes.
mapping = NULL
if(!update & file.exists(paste0(root.dir, "Models/", map, "/COVIDminer_mapping.csv"))){
	mapping = read.csv(paste0(root.dir, "Models/", map, "/COVIDminer_mapping.csv"))
	colnames(mapping) = c("ID", "Name", "CMID")
	rownames(mapping) = mapping$ID
	mapping = mapping[mapping$CMID != "", ]
	if(!all(mapping$Label %in% base.network$vertices[rownames(model), "Label"])) mapping = NULL # If the mapping info does not match the current model, then re-run the search.
}
if(update | is.null(mapping)){
	message("Searching COVIDminer for matching entities...")
	cmsearch = COVIDminerAPI::api_search(query = base.network$vertices[rownames(model), "Label"])
	write.csv(cbind(rownames(model), base.network$vertices[rownames(model), "Name"], unlist(cmsearch$results$`best-match`)), row.names = FALSE, file = paste0(root.dir, "Models/", map, "/COVIDminer_mapping.csv"))
	mapping = read.csv(paste0(root.dir, "Models/", map, "/COVIDminer_mapping.csv"))
	colnames(mapping) = c("ID", "Name", "CMID")
	rownames(mapping) = mapping$ID
	mapping = mapping[mapping$CMID != "", ]
	message("...done")
}
base.network$vertices$CMID = NA
base.network$vertices[na.omit(mapping$ID), "CMID"] = mapping[na.omit(mapping$ID), "CMID"]
model$ID = mapping[rownames(model), "CMID"]
```

```{r covidminernetworkretrieval, echo=FALSE, results='hide'}
# Then use the mapped IDs to retrieve the 1st-level COVIDminer network surrounding them.
session = COVIDminerAPI::api_get(query = mapping$CMID[mapping$CMID != ""], filters = "default", level = 1, unmapped = 0, meta = T, timeout = 1000)
cm.mapping = data.frame(ID = NA, Name = c(session$results$source_label, session$resultstarget_label), "CMID" = c(session$results$source, session$resultstarget))
cm.mapping = cm.mapping[!duplicated(cm.mapping$CMID), ] # NOTE: CMIDs can be associated with more than one alternative name/label. Here we just pick the first one for display.
rownames(cm.mapping) = cm.mapping$CMID
# TODO Check this step - how should the uniqueness of entities be determined?
mapping = rbind(mapping, cm.mapping[!cm.mapping$CMID %in% mapping$CMID, ])
message("Data aggregation complete.")
```

```{r edgelistassembly, echo=FALSE, results='hide'}
edge.list = data.frame(Source = cm.mapping[session$results$source, "Name"], Relation = session$results$relation, Target = cm.mapping[session$results$target, "Name"], Weight = session$results$edge_weight, CMSource = session$results$source, CMTarget = session$results$target, SourceClass = session$results$source_class, TargetClass = session$results$target_class)
# Remove targets that are not in the original model. This workflow does not aim to introduce new outputs.
edge.list = edge.list[edge.list$Target %in% base.network$vertices[rownames(model), "Name"], ]
# Remove edges that are already covered by the original model.
base.edges = paste(
	base.network$vertices[base.network$edges$Source, "CMID"],
	base.network$edges$Relation,
	base.network$vertices[base.network$edges$Target, "CMID"],
	sep = " "
)
new.edges = paste(
	edge.list$CMSource,
	edge.list$Relation,
	edge.list$CMTarget,
	sep = " "
)
edge.list = edge.list[!new.edges %in% base.edges, ]
# Add human-readable edge names.
edge.list$Name = paste(edge.list$Source, c(positive = "->", negative = "-|")[edge.list$Relation], edge.list$Target)
# Make edges unique (don't run the models etc. for the same edge just because it appears in the literature multiple times).
edge.list = unique(edge.list)
# Sort by edge weight descending.
edge.list = edge.list[order(edge.list$Weight, decreasing = TRUE), ]
```

```{r networkassembly, echo=FALSE, results='hide'}
if(nrow(edge.list) > 0){
	# Create a network object from the new edgelist.
	new.network = assemble_network(cbind(edge.list, model = "new"))
	# Add name and label columns like used in the base network.
	new.network$vertices$Name = new.network$vertices$ID
	new.network$vertices$ID = gsub("[^a-zA-Z0-9_]+", "_", new.network$vertices$Name)
	new.network$vertices$Label = new.network$vertices$Name
	# Add class.
	new.class = unique(rbind(cbind(new.network$edges$Source, new.network$edges$SourceClass), cbind(new.network$edges$Target, new.network$edges$TargetClass)))
	new.network$vertices[new.class[, 1], "Class"] = new.class[, 2]
	message("Network assembly complete.")
}
```

```{r modelmodification, echo=FALSE, results='hide'}
if(nrow(edge.list) > 0){
	# Create a modified model for each new interaction.
	# OR
	if(update | !file.exists(paste0(root.dir, "Models/", map, "/modified_models_OR.RData"))){
		modified.models.or = apply(edge.list, 1, function(edge){
			# Search for source by CMID; otherwise it is unique so just add it in.
			source = rownames(model[which(model$ID == as.character(edge["CMSource"])), ])
			if(length(source) > 1){
				is.gene = model[source, "Name"] %in% base.network$vertices[base.network$vertices$Class == "gene", "Name"]
				source = model[source, "Name"][is.gene]
			}
			if(length(source) == 0) source = as.character(edge["Source"][1])
			relation = as.character(edge["Relation"])
			# Target should always be in COVIDminer by definition.
			target = rownames(base.network$vertices[which(base.network$vertices$CMID == as.character(edge["CMTarget"])), ]) 
			if(length(target) > 1) target = target[order(match(base.network$vertices[target, "Class"], c("gene", "trait", "complex", "chemical")))][1] # Reorder to give priority to genes/proteins.
			if(length(target) > 0){ # There are obscure cases where the target has a corrupt ID.
			return(get_modified_model(model, source, relation, target, sourceid = as.character(edge["CMSource"]), rule = "OR"))
			}else{
				return(NULL)
			}
		})
		names(modified.models.or) = edge.list$Name
		modified.models.or = modified.models.or[!sapply(modified.models.or, is.null)]
		save(modified.models.or, file = paste0(root.dir, "Models/", map, "/modified_models_OR.RData"))
	}
	load(paste0(root.dir, "Models/", map, "/modified_models_OR.RData"))
	# AND
	if(update | !file.exists(paste0(root.dir, "Models/", map, "/modified_models_AND.RData"))){
		modified.models.and = apply(edge.list, 1, function(edge){
			# Search for source by CMID; otherwise it is unique so just add it in.
			source = rownames(model[which(model$ID == as.character(edge["CMSource"])), ])
			if(length(source) > 1){
				is.gene = model[source, "Name"] %in% base.network$vertices[base.network$vertices$Class == "gene", "Name"]
				source = model[source, "Name"][is.gene]
			}
			if(length(source) == 0) source = as.character(edge["Source"][1])
			relation = as.character(edge["Relation"])
			# Target should always be in COVIDminer by definition.
			target = rownames(base.network$vertices[which(base.network$vertices$CMID == as.character(edge["CMTarget"])), ])
			if(length(target) > 1) target = target[order(match(base.network$vertices[target, "Class"], c("gene", "trait", "complex", "chemical")))][1] # Reodrder to give priority to genes/proteins.
			if(length(target) > 0){ # There are obscure cases where the target has a corrupt ID.
			return(get_modified_model(model, source, relation, target, sourceid = as.character(edge["CMSource"]), rule = "AND"))
			}else{
				return(NULL)
			}
		})
		names(modified.models.and) = edge.list$Name
		modified.models.and = modified.models.and[!sapply(modified.models.and, is.null)]
		save(modified.models.and, file = paste0(root.dir, "Models/", map, "/modified_models_AND.RData"))
	}
	load(paste0(root.dir, "Models/", map, "/modified_models_AND.RData"))
}
```

```{r modelruleswriteout, echo=FALSE, results='hide'}
if(nrow(edge.list) > 0){
	# Write out Boolean rulesets for inspection.
	unlink(paste0(root.dir, "Models/", map, "/Rulesets/"), recursive = TRUE)
	dir.create(paste0(root.dir, "Models/", map, "/Rulesets/"))
	writeLines(capture.output(print(model)), con = paste0(root.dir, "Models/", map, "/Rulesets/Base_model.txt"))
	# OR
	. = lapply(modified.models.or, function(m){
		m$target = gsub("[^a-zA-Z0-9_]+", "_", m$Name)
		m$rules = m$Rule
		for(x in rownames(m)){
			for(y in rownames(m)){
				m[x, "rules"] = gsub(paste0("\\b", y, "\\b"), m[y, "target"], m[x, "rules"])
			}
		}
		writeLines(paste(m$target, " = ", m$rules), con = paste0(root.dir, "Models/", map, "/Rulesets/", attr(m, "modification"), "_OR.txt"))
	})
	# AND
	. = lapply(modified.models.and, function(m){
		m$target = gsub("[^a-zA-Z0-9_]+", "_", m$Name)
		m$rules = m$Rule
		for(x in rownames(m)){
			for(y in rownames(m)){
				m[x, "rules"] = gsub(paste0("\\b", y, "\\b"), m[y, "target"], m[x, "rules"])
			}
		}
		writeLines(paste(m$target, " = ", m$rules), con = paste0(root.dir, "Models/", map, "/Rulesets/", attr(m, "modification"), "_AND.txt"))
	})
}
```


```{r modelsimulation, echo=FALSE, results='hide'}
figureheight = 1
if(nrow(edge.list) > 0){
	# Get the attractors.
	pbapply::pboptions(char = "==")
	# OR
	if(update | !file.exists(paste0(root.dir, "Models/", map, "/attractors_OR.RData"))){
		cluster = parallel::makeForkCluster(parallel::detectCores())
		attractors.or = do.call("rbind", pbapply::pblapply(c(list("Baseline" = model), modified.models.or), function(m){
			expressions = get_expressions(m)
			simulations = apply(input.states[1:length(expressions), , drop= F], 2, function(input.state){
				x = get_attractor(expressions, input.state)
				colMeans(x[attr(x, "attractor"):nrow(x), , drop = FALSE]) # Mean of all states in the attractor.
			})
			if(nrow(input.states) > nrow(simulations)) simulations = rbind(simulations, rep(NA, n.perm))
			return(rowMeans(simulations))
		}, cl = cluster))
		parallel::stopCluster(cluster)
		save(attractors.or, file = paste0(root.dir, "Models/", map, "/attractors_OR.RData"))
	}
	load(paste0(root.dir, "Models/", map, "/attractors_OR.RData"))
	max.distance = sqrt(sum((rep(0, ncol(attractors.or)) - rep(1, ncol(attractors.or))) ^ 2))
	distance.or = apply(attractors.or, 1, function(x) sqrt(sum((x - attractors.or["Baseline", ]) ^ 2, na.rm = TRUE)) / max.distance )
	# AND
	if(update | !file.exists(paste0(root.dir, "Models/", map, "/attractors_AND.RData"))){
		cluster = parallel::makeForkCluster(parallel::detectCores())
		attractors.and = do.call("rbind",pbapply::pblapply(c(list("Baseline" = model), modified.models.and), function(m){
			expressions = get_expressions(m)
			simulations = apply(input.states[1:length(expressions), , drop= F], 2, function(input.state){
				x = get_attractor(expressions, input.state)
				colMeans(x[attr(x, "attractor"):nrow(x), , drop = FALSE]) # Mean of all states in the attractor.
			})
			if(nrow(input.states) > nrow(simulations)) simulations = rbind(simulations, rep(NA, n.perm))
			return(rowMeans(simulations))
		}, cl = cluster))
		parallel::stopCluster(cluster)
		save(attractors.and, file = paste0(root.dir, "Models/", map, "/attractors_AND.RData"))
	}
	load(paste0(root.dir, "Models/", map, "/attractors_AND.RData"))
	max.distance = sqrt(sum((rep(0, ncol(attractors.and)) - rep(1, ncol(attractors.and))) ^ 2))
	distance.and = apply(attractors.and, 1, function(x) sqrt(sum((x - attractors.and["Baseline", ]) ^ 2, na.rm = TRUE)) / max.distance )
	figureheight = nrow(attractors.or)/2
}
```

<hr>

# Graph
```{r graph, echo=FALSE, fig.width=10, fig.height=10}
if(nrow(edge.list) > 0){
	# Run topology analysis.
	G = igraph::graph_from_edgelist(as.matrix(base.network$edges[, c("Source", "Target")]))
	betweenness = igraph::betweenness(G, directed = TRUE)
	base.clustering = igraph::transitivity(G, type = "global")
	sensitivity = abs((sapply(igraph::V(G), function(v){
		igraph::transitivity(igraph::delete_vertices(G, v), type = "global")
	}) - base.clustering) / base.clustering)
	base.network$vertices$betweenness = betweenness[rownames(base.network$vertices)]
	base.network$vertices$sensitivity = sensitivity[rownames(base.network$vertices)]
	
	# Merge base and new networks into a master network.
	network = network_union(list(base.network, new.network))
	
	coordinates = network$vertices[, c("x", "y")]
	# Scale the network for proper layout.
	base.x = range(coordinates$x, na.rm = T) 
	base.y = range(coordinates$y, na.rm = T)
	coordinates$x = (coordinates$x - min(base.x)); coordinates$x = coordinates$x / (max(base.x) - min(base.x))
	coordinates$y = (coordinates$y - min(base.y)); coordinates$y = coordinates$y / (max(base.y) - min(base.y))
	# Set unknowns to origin.
	coordinates[is.na(coordinates)] = 0.5
	coordinates = round(coordinates, 4)
	
	# Plot the graph using Cytoscape.js.	
	get.vertex.class = function(x){
		class = "unknown"
		if(x %in% input.nodes) class = "input"
		if(x %in% output.nodes) class = "output"
		if(!x %in% rownames(base.network$vertices)) class = paste(class, "new")
		return(class)
	}
	
	get.vertex.lock = function(x){
		locked = "false"
		if(x %in% rownames(base.network$vertices)) locked = "true"
		return(locked)
	}
	
	cyjs.script = paste0('<script>
			var cy;
			(function(){
				document.addEventListener("DOMContentLoaded", function(){
						bb = document.getElementById("cy").getBoundingClientRect();
		
						// Elements are constructed from database search results
						var graphData = [',
			
						paste(sapply(rownames(coordinates), function(v){		
								paste0('{"data": {"id": "', v, '", "label": "', network$vertices[v, "Label"], '", "score": 0, "query": false},"position": {"x": ((bb.right - bb.left) * ', coordinates[v, "x"], '), "y": ((bb.top - bb.bottom) * ', coordinates[v, "y"], ')},"group": "nodes", "removed": false, "selected": false, "selectable": true, "locked": ', get.vertex.lock(v), ', "grabbable": true, "classes": "', get.vertex.class(v), '"}',
							collapse = "")
						}), collapse = ",\n"),
						',',
						paste(sapply(rownames(network$edges), function(e){		
								paste0('{"data": {"id": "', e, '", "source": "', network$edges[e, "Source"], '", "target": "', network$edges[e, "Target"], '", "label": "", "weight": "1", "relation": "', network$edges[e, "Relation"], '"},"position": {}, "group": "edges", "removed": false, "selected": false, "selectable": true, "locked": false, "grabbable": true, "classes": "',  paste(network$edges[e, "Relation"], network$edges[e, "model"]), '"}',
							collapse = "")
						}), collapse = ",\n"),
	
						'];			
				
						// Get style via ajax
						graphStyle = "', paste(readLines("exaf-covid.cycss"), collapse = ""), '";
				
						// Container (and initialisation)
						cy = cytoscape({
							container: document.getElementById("cy"),
							elements: graphData,
							style: graphStyle,
							layout: { name: "fcose", boundingBox: {x1: bb.left, y1: bb.top, x2: bb.right, y2: bb.bottom}, randomize: false, animate: false, idealEdgeLength: 35, fixedNodeConstraint: [',
							paste(sapply(rownames(base.network$vertices), function(v) paste0('{nodeId: "', v, '", position: {x: ', coordinates[v, "x"], ', y: ', coordinates[v, "y"], '}}', collapse = "") ), collapse = ","),
	  					']}
						});
					});
			})();
			$(document).ready(function(){
				cy.userZoomingEnabled(false);
			});
		</script>')
}else{
	cyjs.script = ""
}
```
`r cyjs.script`
<div id="cy"></div>

<hr>

# Modelling
The newly-found interactions are added one at a time to the model using an OR rule to the existing ruleset.
For modelling, all node states are set initially to 0, and the state of the input nodes is set randomly for each iteration (in fact the series random starting states is the same for each new model, so the models for each new interaction are comparable).
The models are simulated and the mean attractor state over the `r ncol(input.states)` iterations is plotted as a heatmap.

## Using an OR rule.

```{r modelsimulationOR, echo=FALSE, fig.width=10, fig.height=figureheight}
if(nrow(edge.list) > 0){
	colour.palette = colorRampPalette(c("#F8F8FF", "#2020B0"))(256)
	rotate = function(content){
		paste0(
			'<div id="container"><div class="rotation-wrapper-outer"><div class="rotation-wrapper-inner"><div class="element-to-rotate">', 
			content,
			'</div></div></div></div>'
		)
	}
	
	# Generate the attractor results as a table.
	table = as.data.frame(attractors.or[, output.nodes, drop = FALSE])
	colnames(table) = model[colnames(table), "Name"]
	
	# Replace ASCII edge arrows with embedded custom font.
	table.names = rownames(table)
	table.names = gsub(" \\-> ", '&nbsp;<span class="icon-positive-arrow"></span>&nbsp;', table.names)
	table.names = gsub(" \\-\\| ", '&nbsp;<span class="icon-negative-arrow"></span>&nbsp;', table.names)
	
	table.start = '<table width="100%" style="padding:32px; border:0px">'
	
	table.header = paste0(
		'<tr style="">',
		'<th class="model-table-column-header">', '</th>',	
		paste0(
			sapply(1:ncol(table), function(col){
				paste0('<th class="model-table-column-header">', colnames(table)[col], '</th>')
			}),
			collapse = " "
		),	
		'</tr>'
	)
	table.body = sapply(1:nrow(table), function(row){
		paste0(
			'<tr>',
			paste0('<td class="model-table-row-header">', table.names[row], '</td>'),
			paste0(
				sapply(1:ncol(table), function(col){
					paste0('<td style="background:', colour.palette[round(255 * table[row, col])], '; border:0px"></td>')
				}),	
				collapse = " "
			),	
			'</tr>'
		)
	})
	table.end = '</table>'
	table = paste(c(table.start, table.header, table.body, table.end), collapse = "\n")
}else{
	table = ""
}
```

`r table`

```{r modelsimulationscalebar, echo=FALSE, fig.width=10, fig.height=1}
if(nrow(edge.list) > 0){
	# Generate the attractor results as a table.
	table = t(c(1, round(255 / 10:1)))
	colnames(table) = 0:10
	
	table.start = '<table width="100%" style="padding:32px; border:2px">'
	
	table.header = paste0(
		'<tr style="">',
		paste(sapply(1:ncol(table), function(col){
			paste0('<th style="border:0px">', colnames(table)[col], '</th>')
		}), collapse = ""),	
		'</tr>'
	)
	table.body = sapply(1:nrow(table), function(row){
		paste0(
			'<tr>',
			paste(sapply(1:ncol(table), function(col){
				paste0('<td style="background:', colour.palette[table[row, col]], '; border:0px"></td>')
			}), collapse = ""),	
			'</tr>'
		)
	})
	table.end = '</table>'
	table = paste(c(table.start, table.body, table.header, table.end), collapse = "\n")
}else{
	table = ""
}
```

`r table`


## Using an AND rule.

```{r modelsimulationAND, echo=FALSE, fig.width=10, fig.height=figureheight}
if(nrow(edge.list) > 0){
	colour.palette = colorRampPalette(c("#F8F8FF", "#2020B0"))(256)
	rotate = function(content){
		paste0(
			'<div id="container"><div class="rotation-wrapper-outer"><div class="rotation-wrapper-inner"><div class="element-to-rotate">', 
			content,
			'</div></div></div></div>'
		)
	}
	
	# Generate the attractor results as a table.
	table = as.data.frame(attractors.and[, output.nodes, drop = FALSE])
	colnames(table) = model[colnames(table), "Name"]
	
	# Replace ASCII edge arrows with embedded custom font.
	table.names = rownames(table)
	table.names = gsub(" \\-> ", '&nbsp;<span class="icon-positive-arrow"></span>&nbsp;', table.names)
	table.names = gsub(" \\-\\| ", '&nbsp;<span class="icon-negative-arrow"></span>&nbsp;', table.names)
	
	table.start = '<table width="100%" style="padding:32px; border:0px">'
	
	table.header = paste0(
		'<tr style="">',
		'<th class="model-table-column-header">', '</th>',	
		paste0(
			sapply(1:ncol(table), function(col){
				paste0('<th class="model-table-column-header">', colnames(table)[col], '</th>')
			}),
			collapse = " "
		),	
		'</tr>'
	)
	table.body = sapply(1:nrow(table), function(row){
		paste0(
			'<tr>',
			paste0('<td class="model-table-row-header">', table.names[row], '</td>'),
			paste0(
				sapply(1:ncol(table), function(col){
					paste0('<td style="background:', colour.palette[round(255 * table[row, col])], '; border:0px"></td>')
				}),	
				collapse = " "
			),	
			'</tr>'
		)
	})
	table.end = '</table>'
	table = paste(c(table.start, table.header, table.body, table.end), collapse = "\n")
}else{
	table = ""
}
```

`r table`

<hr>

# Links
```{r links, echo=FALSE}
if(nrow(edge.list) > 0){
	# Generate a table of edges linked to COVIDminer as clickable links.
	topology = base.network$vertices[match(edge.list$CMTarget, base.network$vertices$CMID), c("betweenness", "sensitivity")]
	
	table = cbind(edge.list[, c("Name", "Weight")], Betweenness = topology$betweenness, Sensitivity = topology$sensitivity)
	write.csv(table, file = paste0(root.dir, "Models/", map, "/ResultsTable.csv"))

	# Add correlations to baseline.
	#table$dOR = distance.or[table$Name]
	#table$dAND = distance.and[table$Name]
	
	# Replace ASCII edge arrows with embedded custom font.
	table$Name = gsub("\\->", '<span class="icon-positive-arrow"></span>', table$Name)
	table$Name = gsub("\\-\\|", '<span class="icon-negative-arrow"></span>', table$Name)
	
	table$Link = paste0(
		"https://rupertoverall.net/covidminer/index?ids=", 
		edge.list$CMSource, 
		",", 
		edge.list$CMTarget, 
		"&level=0"
	)
	table$Colour = rep(c("#F5F5F5", "#FFFFFF") , length.out = nrow(table))
	
	table.start = '<table width="100%" style="padding:32px">'
	table.header = paste0(
		'<tr style="background:#305080; color:white; font-weight: bold">',
		'<th>', '</th>',	
		'<th>', "Interaction", '</th>',	
		'<th>', "Weight", '</th>',	
		'<th>', "Target\nBetweeness", '</th>',	
		'<th>', "Target\nSensitivity", '</th>',	
	#	'<th>', "OR <i>r<sup>2</sup></i>", '</th>',	
	#	'<th>', "AND <i>r<sup>2</sup></i>", '</th>',	
		'</tr>'
	)
	table.body = sapply(1:nrow(table), function(row){
		paste0(
			'<tr style="background:', table[row, "Colour"], '">',
			paste0(
				'<td>', row, '</td>',	
				'<td>', '<a href="', table[row, "Link"], '">', table[row, "Name"], '</a></td>',	
				'<td>', table[row, "Weight"], '</td>',	
				'<td>', signif(table[row, "Betweenness"], 2), '</td>',	
				'<td>', signif(table[row, "Sensitivity"], 2), '</td>',	
	#			'<td>', format(round(table[row, "dOR"], 2), nsmall = 2), '</td>',	
	#			'<td>', format(round(table[row, "dAND"], 2), nsmall = 2), '</td>',	
				collapse = " "
			),	
			'</tr>'
		)
	})
	table.end = '</table>'
	table = paste(c(table.start, table.header, table.body, table.end), collapse = "\n")
}else{
	table = ""
}
```
`r table`

<br>

<hr>

