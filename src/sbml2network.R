sbml2network = function(filename){
	smbl = xml2::as_list(xml2::read_xml(filename))

	layout = smbl$sbml$model$listOfLayouts$layout$listOfAdditionalGraphicalObjects
	layout = as.data.frame(do.call("rbind", lapply(layout, function(glyph){
		ref = attr(glyph, "reference")
		id = attr(glyph, "id")
		x = attr(glyph$boundingBox$position, "x")
		y = attr(glyph$boundingBox$position, "y")
		width = attr(glyph$boundingBox$dimensions, "width")
		height = attr(glyph$boundingBox$dimensions, "height")
		return(cbind(ID = id, Ref = ref, x = x, y = y, width = width, height = height))
	})))
	rownames(layout) = layout$Ref

	species = smbl$sbml$model$listOfQualitativeSpecies
	species = as.data.frame(do.call("rbind", lapply(species, function(sp){
		id = attr(sp, "id")
		name = attr(sp, "name")
		compartment = attr(sp, "compartment")
		ref = attr(sp$annotation$RDF$Description, "about")

		casqrule = as.character(na.omit(sapply(sp$annotation$RDF$Description, function(desc){
			resource = attr(desc$Bag$li,"resource")
			if(is.null(resource)) resource = NA
			if(grepl("urn\\:casq\\:function\\:", resource)){
				resource = sub("urn\\:casq\\:function\\:", "",  resource)
			}else{
				resource = NA
			}
			return(resource)
		})))
		identifiers = as.character(na.omit(sapply(sp$annotation$RDF$Description, function(desc){
			resource = attr(desc$Bag$li,"resource")
			if(is.null(resource)) resource = NA
			if(grepl("urn\\:miriam\\:", resource)){
				resource = sub("urn\\:miriam\\:", "",  resource)
			}else{
				resource = NA
			}
			return(resource)
		})))
		identifiers = paste(unlist(identifiers), collapse = ";")
		return(c(ID = name, Name = name, SbmlId = id, Compartment = compartment, Ref = ref, Identifiers = identifiers))
	})))
	rownames(species) = species$SbmlId

	species = as.data.frame(cbind(species, layout[rownames(species), c("x", "y", "width", "height")]))
	rownames(species) = paste0("v", 1:nrow(species))

	edge.list = smbl$sbml$model$listOfTransitions
	edge.list = as.data.frame(do.call("rbind", lapply(edge.list, function(transition){
		inputs = do.call("rbind", lapply(transition$listOfInputs, function(input){
			x = rownames(species)[species$SbmlId == attr(input, "qualitativeSpecies")]
			effect = attr(input, "transitionEffect")
			sign = attr(input, "sign")
			return(c(Source = x, Relation = sign))
		}))
		outputs = do.call("rbind", lapply(transition$listOfOutputs, function(output){
			x = rownames(species)[species$SbmlId == attr(output, "qualitativeSpecies")]
			effect = attr(output, "transitionEffect")
			return(c(Target = x))
		}))
		rules = unname(do.call("c", lapply(transition$listOfFunctionTerms$functionTerm$math, function(math){
			return(parse_mathml(math, species))
		})))
		ref = attr(transition$annotation$RDF$Description, "about")
		if(is.null(ref)) ref = NA
		result = data.frame(inputs, outputs, Ref = ref, Rule = rules)
		return(result)
	})))
	rownames(edge.list) = NULL

	# Rules are stored under transitions, even though defined for each species.
	# Extract the unique rulesets and transfer them to the vertices.
	rules = unique(edge.list[, c("Target", "Rule")])
	species$Rule = rownames(species) # By default, inputs must activate themselves if no other rule defined.
	species[match(rules$Target, rownames(species)), "Rule"] = rules$Rule
	edge.list$Rule = NULL

	network = list(
		vertices = species,
		edges = as.data.frame(edge.list)
	)
	network$edges = unique(network$edges)
	rownames(network$edges) = paste0("e", 1:nrow(network$edges))
	vertex.ids = rownames(network$vertices)
	network$edges$Source = vertex.ids[match(network$edges$Source, rownames(network$vertices))]
	network$edges$Target = vertex.ids[match(network$edges$Target, rownames(network$vertices))]
	network$vertices$x = as.numeric(network$vertices$x)
	network$vertices$y = as.numeric(network$vertices$y)
	network$vertices$width = as.numeric(network$vertices$width)
	network$vertices$height = as.numeric(network$vertices$height)

	return(network)
}
